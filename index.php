<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.css">
  <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.js"></script>
  <title>Json</title>
</head>

<body>
  <?php

  function csvToJson($fname) {
  if (!($fp = fopen($fname, 'r'))) {
  die("Can't open file...");
}
$key = fgetcsv($fp,"1024",",");
$json = array();
while ($row = fgetcsv($fp,"1024",",")) {
$json[] = array_combine($key, $row);
}
fclose($fp);
return json_encode($json);
}
?>


<?php
$data=(csvToJson("./exo.csv"));
$obj = json_decode($data); 
?>
<div class="ui three column grid container">
  <br>
  <h1>Computer science figures</h1>
  <div class="ui link centered cards">
    <div class="card">
      <div class="image">
        <?php
        echo '<img src="'.$obj[0]->picture.'" alt="" />';
        ?>
      </div>
      <div class="content">
        <div class="header">
          <?php echo $obj[0]->name; ?>
        </div>
        <div class="meta">
          <a><?php echo $obj[0]->title; ?></a>
        </div>
        <div class="description">
          <?php echo $obj[0]->role; ?>
        </div>
      </div>
      <div class="extra content">
        <span class="right floated">
          Born in <?php echo $obj[0]->birthyear; ?>
        </span>
      </div>
    </div>
    <div class="card">
      <div class="image">
        <?php
        echo '<img src="'.$obj[1]->picture.'" alt="" />';
        ?>
      </div>
      <div class="content">
        <div class="header">
          <?php echo $obj[1]->name; ?>
        </div>
        <div class="meta">
          <a><?php echo $obj[1]->title; ?></a>
        </div>
        <div class="description">
          <?php echo $obj[1]->role; ?>
        </div>
      </div>
      <div class="extra content">
        <span class="right floated">
          Born in <?php echo $obj[1]->birthyear; ?>
        </span>
      </div>
    </div>
    <div class="card">
      <div class="image">
        <?php
        print '<img src="'.$obj[2]->picture.'" alt="" />';
        ?>
      </div>
      <div class="content">
        <div class="header">
          <?php echo $obj[2]->name; ?>
        </div>
        <div class="meta">
          <a><?php echo $obj[2]->title; ?></a>
        </div>
        <div class="description">
          <?php echo $obj[2]->role; ?>
        </div>
      </div>
      <div class="extra content">
        <span class="right floated">
         Born in <?php echo $obj[2]->birthyear; ?>
       </span>
     </div>
   </div>
   <div class="card">
    <div class="image">
      <?php
      print '<img src="'.$obj[3]->picture.'" alt="" />';
      ?>
    </div>
    <div class="content">
      <div class="header">
        <?php echo $obj[3]->name; ?>
      </div>
      <div class="meta">
        <a><?php echo $obj[3]->title; ?></a>
      </div>
      <div class="description">
        <?php echo $obj[3]->role; ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
        Born in <?php echo $obj[3]->birthyear; ?>
      </span>
    </div>
  </div>
  <div class="card">
    <div class="image">
      <?php
      print '<img src="'.$obj[4]->picture.'" alt="" />';
      ?>
    </div>
    <div class="content">
      <div class="header">
        <?php echo $obj[4]->name; ?>
      </div>
      <div class="meta">
        <a><?php echo $obj[4]->title; ?></a>
      </div>
      <div class="description">
        <?php echo $obj[4]->role; ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
        Born in <?php echo $obj[4]->birthyear; ?>
      </span>
    </div>
  </div>
  <div class="card">
    <div class="image">
      <?php
      print '<img src="'.$obj[5]->picture.'" alt="" />';
      ?>
    </div>
    <div class="content">
      <div class="header">
        <?php echo $obj[5]->name; ?>
      </div>
      <div class="meta">
        <a><?php echo $obj[5]->title; ?></a>
      </div>
      <div class="description">
        <?php echo $obj[5]->role; ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
        Born in <?php echo $obj[5]->birthyear; ?>
      </span>
    </div>
  </div>
  <div class="card">
    <div class="image">
      <?php
      print '<img src="'.$obj[6]->picture.'" alt="" />';
      ?>
    </div>
    <div class="content">
      <div class="header">
        <?php echo $obj[6]->name; ?>
      </div>
      <div class="meta">
        <a><?php echo $obj[6]->title; ?></a>
      </div>
      <div class="description">
        <?php echo $obj[6]->role; ?>
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
        Born in <?php echo $obj[6]->birthyear; ?>
      </span>
    </div>
  </div>
</div>
</div>

</body>
</html>